import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {filter, first, map, mergeMap, switchMap, takeUntil} from 'rxjs/operators';
import {DeviceTypeEnum} from '../types/device-type.enum';
import {ResizeService} from '../services/resize.service';
import {ImageService} from '../services/image.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'carousel-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarouselComponent implements OnInit, OnDestroy {
  images$ = new BehaviorSubject([]);
  selectedImages$: Observable<number[]>;

  selectedImage$ = new BehaviorSubject<number>(0);

  form: FormGroup;

  error: string;

  private unsubscribeOnImages$ = new Subject<void>();

  constructor(
    private resizeService: ResizeService,
    private imageService: ImageService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      apiKey: ['', Validators.required],
      keyword: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.selectedImages$ = this.images$
      .pipe(
        takeUntil(this.unsubscribeOnImages$),
        filter(list => list.length > 0),
        switchMap((images: number[]) => this.getSelectedImagesArraySize$()
          .pipe(switchMap(size => this.selectedImage$.asObservable()
            .pipe(map(index => this.buildSelectedArray(images, size, index))
            ))
          )
        )
      );
  }

  ngOnDestroy(): void {
    this.unsubscribeOnImages$.next();
    this.unsubscribeOnImages$.complete();
    this.selectedImage$.complete();
  }

  onSearch(): void {
    if (this.form.valid) {
      this.imageService.getImages(
        this.form.get('apiKey').value,
        this.form.get('keyword').value)
        .subscribe(
          images => {
            this.images$.next(images);
            if (images.length === 0) {
              this.error = 'No images found';
            }
          },
          () => {
            this.error = 'Something went wrong, check your api key';
            this.images$.next([]);
          }
        );
    } else {
      this.error = 'Need to fill in both inputs';
    }
  }

  next(): void {
    this.images$.pipe(
      mergeMap(images => this.selectedImage$.asObservable()
        .pipe(map(index => images.length - 1 === index ? 0 : index + 1))
      ),
      first()
    ).subscribe(index => this.selectedImage$.next(index));
  }

  previous(): void {
    this.images$.pipe(
      mergeMap(images => this.selectedImage$.asObservable()
        .pipe(map(index => 0 === index ? images.length - 1 : index - 1))
      ),
      first()
    ).subscribe(index => this.selectedImage$.next(index));
  }

  private getSelectedImagesArraySize$(): Observable<number> {
    return this.resizeService.onDeviceChanged$
      .pipe(map((deviceType: DeviceTypeEnum) => {
        switch (deviceType) {
          case DeviceTypeEnum.SMALL:
            return 1;
          case DeviceTypeEnum.MEDIUM:
            return 3;
          default:
            return 5;
        }
      }));
  }

  private buildSelectedArray(images: number[], selectedSize: number, selectedIndex: number): number[] {
    const selectedImages = new Array(selectedSize);
    const offset = Math.floor(selectedSize / 2);
    selectedImages[offset] = images[selectedIndex];

    for (let i = 1; i <= offset; i++) {
      if (selectedIndex - i >= 0) {
        selectedImages[offset - i] = images[selectedIndex - i];
      }

      if (selectedIndex + i < images.length) {
        selectedImages[offset + i] = images[selectedIndex + i];
      }
    }
    return selectedImages;
  }
}
