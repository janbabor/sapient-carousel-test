import {TestBed} from '@angular/core/testing';

import {ImageService} from './image.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {isArray} from 'util';

describe('ImageService', () => {
  let service: ImageService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  beforeEach(() => {
    service = TestBed.get(ImageService);
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getImages', () => {
    it('should return an array of urls', () => {
      const apiKey = 'apiKey';
      const keyword = 'keyword';
      const imageURL = 'imageURL';
      const testData = {hits: [{imageURL}]};

      service.getImages(apiKey, keyword).subscribe(data => {
        expect(isArray(data)).toBeTruthy();
        data.map(url => expect(url).toEqual(imageURL));
      });

      const req = httpTestingController.expectOne(`https://pixabay.com/api/?key=${apiKey}&q=${keyword}`);
      expect(req.request.method).toEqual('GET');
      req.flush(testData);
      httpTestingController.verify();
    });
  });
});
