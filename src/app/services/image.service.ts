import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {PixabaySearchResponse} from '../types/pixabay-search-response';

/**
 * A service which provides functions to work with the Pixabay API
 */
@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private httpClient: HttpClient) {
  }

  getImages(apiKey: string, keyword: string): Observable<string[]> {
    return this.httpClient.get(`https://pixabay.com/api/`, {
      params: {
        key: apiKey,
        q: keyword
      }
    }).pipe(
      map((response: PixabaySearchResponse) => response.hits.map(h => h.imageURL || h.largeImageURL))
    );
  }
}
