import {TestBed} from '@angular/core/testing';

import {ResizeService} from './resize.service';
import {DeviceTypeEnum} from '../types/device-type.enum';

describe('ResizeService', () => {
  let service: ResizeService;

  beforeEach(() => TestBed.configureTestingModule({}));

  beforeEach(() => service = TestBed.get(ResizeService));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('size$', () => {
    it('should return a numbered value', () => {
      service.resize(300);

      service.size$.subscribe(value =>
        expect(typeof value === 'number').toBeTruthy()
      );
    });
  });

  describe('onDeviceChanged$', () => {
    it('should return a a small device type', () => {
      service.resize(300);
      service.onDeviceChanged$.subscribe(value =>
        expect(value).toBe(DeviceTypeEnum.SMALL)
      );
    });

    it('should return a a medium device type', () => {
      service.resize(500);
      service.onDeviceChanged$.subscribe(value =>
        expect(value).toBe(DeviceTypeEnum.MEDIUM)
      );
    });

    it('should return a a large device type', () => {
      service.resize(1400);
      service.onDeviceChanged$.subscribe(value =>
        expect(value).toBe(DeviceTypeEnum.LARGE)
      );
    });
  });
});
