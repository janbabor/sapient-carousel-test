import {Injectable} from '@angular/core';
import {DeviceTypeEnum} from '../types/device-type.enum';
import {EventManager} from '@angular/platform-browser';
import {BehaviorSubject, Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';

/**
 * A service which keeps track of the window size and resize events.
 */
@Injectable({
  providedIn: 'root'
})
export class ResizeService {
  private resize$ = new BehaviorSubject<number>(undefined);

  onResize = (event: UIEvent) => this.resize((event.target as Window).innerWidth);

  get size$(): Observable<number> {
    return this.resize$
      .asObservable()
      .pipe(
        filter(Boolean)
      );
  }

  get onDeviceChanged$(): Observable<DeviceTypeEnum> {
    return this.size$
      .pipe(
        map(width => {
          if (width <= 480) {
            return DeviceTypeEnum.SMALL;
          } else if (width <= 1024) {
            return DeviceTypeEnum.MEDIUM;
          }
          return DeviceTypeEnum.LARGE;
        })
      );
  }

  constructor(private eventManager: EventManager) {
    this.resize$.next(window.innerWidth);
    eventManager.addGlobalEventListener('window', 'resize', this.onResize);
  }

  resize(width: number) {
    this.resize$.next(width);
  }
}
