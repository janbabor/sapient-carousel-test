import {PixabaySearchHit} from './pixabay-search-hit';

export interface PixabaySearchResponse {
  total: number;
  totalHits: number;
  hits: PixabaySearchHit[];
}
